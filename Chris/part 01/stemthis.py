"""
	CT422 - Assignment 1
	 
	c.loughnane1@nuigalway.ie - 09101916

	Using porter2.py 
	Adapted from pyporter2 by Michael Dirolf.
"""

from porter2 import stem
from string import ascii_letters, digits

import os

def loadStopWords():
	with open('stopwords.txt', 'r') as f:
	    #return [line.strip() for line in f]
	    x = set(f)
	    return map(lambda s: s.strip(), x)

def loadSample():
	return open(os.path.join(os.getcwd(), 'sample1.txt'), 'r').read()

""" 
	cleans out non ascii alphanumerics
"""
def removeNonAlphnumeric(s): 
	return "".join(i for i in s if i in (ascii_letters + digits)).lower()

def cleanSample():
	return " ".join(removeNonAlphnumeric(word) for word in sample.split(" "))

"""
	Removes stops words and stems sample
"""
def stemAndRemoveStop():
	return " ".join(stem(word) for word in cleanedSample.split(" ") if word not in stopwords)

os.system('cls')

stopwords = loadStopWords()

sample = loadSample()

cleanedSample = cleanSample()

treatedText = stemAndRemoveStop()

#print stopwords
print "*********************** Sample Text ***********************\n"
print sample
print "\n*************** Stop words and stemmed Text ***************\n"
print treatedText

print "\n\nCharacter count of sample ", sample.__len__()
print "\nCharacter count of treated text", treatedText.__len__()

print "\nThe compression rate is ", (float(sample.__len__())/treatedText.__len__())

