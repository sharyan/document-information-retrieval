'''
Created on 5 Oct 2013

@author: Shane
'''
import unittest
from Utility import parsePlaintext, parseXML, parseJSON

class ParseTests(unittest.TestCase):

    def setUp(self):
        self.plaintext = str(open("../../resources/sample.txt").readlines())
        self.xml = open("../..resources/sample.xml").readlines()
        self.json = "../../resources/sample.json"
        
    def test_parsePlaintext(self):
        results = parsePlaintext(self.plaintext)
        self.checkResults(results, "Test failed for plaintext parse")

    def test_parseJSON(self):
        results = parseJSON(self.json)
        self.checkResults(results, "Test failed for json parse")

    def test_parseXML(self):
        results = parseXML(self.xml)
        self.checkResults(results, "Test failed for xml parse")

    def checkResults(self, results, msg):
        self.assertTrue(("citizenry" in s for s in results), msg)
        self.assertTrue(("multitude" in s for s in results), msg)
        self.assertTrue(("masses" in s for s in results), msg)
        self.assertTrue(("mass" in s for s in results), msg)
        self.assertTrue(("hoi polloi" in s for s in results), msg)
        self.assertTrue(("the great unwashed" in s for s in results), msg)
        self.assertTrue(("family" in s for s in results), msg)
        self.assertTrue(("folk" in s for s in results), msg)
        self.assertTrue(("group" in s for s in results), msg)
        self.assertTrue(("grouping" in s for s in results), msg)
        self.assertTrue(("kinfolk" in s for s in results), msg)
        self.assertTrue(("populate" in s for s in results), msg)



if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()