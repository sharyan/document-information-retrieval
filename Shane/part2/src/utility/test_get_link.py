import unittest
from Utility import get_link

class GetLinkTests(unittest.TestCase):

    def test_getPlaintextLink(self):
        plaintext = get_link("short")
        self.assertEqual("http://words.bighugelabs.com/api/2/ccb1381eb6eec1f819739a10923d6942/short/", plaintext)

    def test_getJsonLink(self):
        json = get_link("short","json")
        self.assertEqual("http://words.bighugelabs.com/api/2/ccb1381eb6eec1f819739a10923d6942/short/json", json)

    def test_getXMLLink(self):
        xml = get_link("short","xml")
        self.assertEqual("http://words.bighugelabs.com/api/2/ccb1381eb6eec1f819739a10923d6942/short/xml", xml)


if __name__ == '__main__':
    unittest.main()
