import re

import xml.etree.ElementTree as ET
import json


link_text = "http://words.bighugelabs.com/api/2/ccb1381eb6eec1f819739a10923d6942/<word>/"
link_xml = "http://words.bighugelabs.com/api/2/ccb1381eb6eec1f819739a10923d6942/<word>/xml"
link_json = "http://words.bighugelabs.com/api/2/ccb1381eb6eec1f819739a10923d6942/<word>/json"


def get_link(word, type="plaintext"):
    """
    Returns a link to the online thesaurus api that will return a list of terms that relate to the term provided.
    :param word: the word to lookup
    :param type: Default to plaintext, the type of data you want the request to give back. Currently supports xml and json
    """
    if(type=="xml"):
        return re.sub("<word>", word, link_xml)
    if(type=="json"):
        return re.sub("<word>", word, link_json)
    return re.sub(r'<word>', word, link_text)


def parseTerms(terms, type="plaintext"):
    """
    Parses the terms returned by the thesaurus into a list
    :param terms: the synonyms in the format designated by the type parameter
    :param type: plaintext, xml, json
    """
    if(type.lower()=="xml"):
        return parseXML(terms)
    if(type.lower()=="json"):
        return parseJSON(terms)
    return parsePlaintext(terms)

def parseXML(terms):
    """
    Returns a list of terms returned by the thesaurus
    :param terms: the terms in xml format
    :return: a list of all the related terms returned from the thesaurus
    """
    results = list()
    tree = ET.fromstringlist(terms)
    for child in tree:
        results.append(child.text)
    return results

def parseJSON(filename):
    """
    Returns a list of terms returned by the thesaurus from json data
    :param filename: file containing terms in json format
    :return: list of terms from the json result
    """
    data = json.load(open(filename))
    return data["noun"]["syn"]

def parsePlaintext(terms):
    """
    Returns a list of terms returned by the thesaurus from plaintext
    :param terms: terms in plaintext format
    :return: list of terms
    """
    results = list()
    p = re.compile('noun\|syn\|[\w|\s]+', re.IGNORECASE)
    found = p.findall(terms)
    for term in found:
        results.append(re.sub("noun\|syn\|", "", term)  )
    return results

