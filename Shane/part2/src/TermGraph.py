'''
Created on 5 Oct 2013

@author: Shane
'''

import networkx as nx

class TermGraph:
    '''
    classdocs
    '''


    def __init__(self):
        '''
        Constructor
        '''
        self.graph = nx.Graph()
        self.numberTerms = 0
        
        
    def addTerm(self, term):
        if not self.graph.has_node(term):
            self.graph.add_node(term)
            self.numberTerms = len(self.graph.nodes())
            self.recalculateWeights()
            
    def recalculateWeights(self):
        edges = self.graph.edges()
        for edge in edges:
            self.graph[edge[0]][edge[1]]['weight'] = round(self.graph[edge[0]][edge[1]]['occurances']/float(self.numberTerms),2)
        
    def addRealtionship(self, term1, term2):
        self.addTerm(term1)
        self.addTerm(term2)
        if self.graph.has_edge(term1, term2):
            if self.graph[term1][term2]['occurances'] is not None:
                self.graph[term1][term2]['occurances'] = self.graph[term1][term2]['occurances'] + 1
                self.graph[term1][term2]['weight'] = round(self.graph[term1][term2]["occurances"]/float(self.numberTerms))
            else:
                self.graph[term1][term2]['occurances'] = 1
                self.graph[term1][term2]['weight'] = 1/self.numberTerms
        else:    
            self.graph.add_edge(term1, term2, {'occurances' : 1, 'weight' : round(1/float(self.numberTerms),2)})

    def has_path(self, term1, term2):
        try:
            if nx.has_path(self.graph, term1, term2) is True:
                return True
            else:
                raise nx.exception.NetworkXError()
        except nx.exception.NetworkXError:
            print "No relation between " + term1 + " and " + term2
            return False
    
    def shortest_path(self, term1, term2):
        return nx.shortest_path(self.graph, term1, term2)
    
    def getRelatedness(self, terms):
        relatedness = 1
        for index,term in enumerate(terms):
            if(len(terms) > index+1):
                edge_data = self.graph.get_edge_data(terms[index], terms[index+1], 0)
                relatedness *= edge_data['weight']
        return relatedness        
                
if __name__ == "__main__":
    # some developer testing code for this class (just seeing what works...)
    graph = TermGraph()
    graph.addRealtionship("shane", "ryan")
    graph.addRealtionship("shane", "ryan")
    graph.addRealtionship("ryan", "peter")
    graph.addRealtionship("ryan", "says")
    