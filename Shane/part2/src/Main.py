'''
Created on 5 Oct 2013

@author: Shane
'''

import os
import nltk
from TermGraph import TermGraph

path = "../resources/documents/"



def getDocumentSet(setName):
    setPath = path + setName
    results = list()
    for f in os.listdir(setPath):
        if f.endswith(".txt"):
            results.append(open(setPath + "/" + f))
    return results
    

def addDocumentToGraph(termGraph, document):
    # for each sentence in the document
        # add the terms to the graph
    terms = ""
    for line in document:
        if line is not None:
            terms = line
            if "." in line:
                terms = line.split(".")
                for sentence in terms:
                    tokenized_terms = nltk.word_tokenize(sentence)
                    for index,term in enumerate(tokenized_terms):
                        if(len(tokenized_terms) > index+1):
                            termGraph.addRealtionship(tokenized_terms[index], tokenized_terms[index+1])
                        elif(len(tokenized_terms) == 1):
                            termGraph.addTerm(tokenized_terms[0])
            else:
                tokenized_terms = nltk.word_tokenize(terms)
                for index,term in enumerate(tokenized_terms):
                    if(len(tokenized_terms) > index+1):
                        termGraph.addRealtionship(tokenized_terms[index], tokenized_terms[index+1])
                

def main():
    set1_graph = TermGraph()
    set1 = getDocumentSet("set1")
    for document in set1:
        addDocumentToGraph(set1_graph, document)
    if set1_graph.has_path("hurricane", "joey"):
        print "Relatedness between hurricane and joey = " +  str(set1_graph.getRelatedness(set1_graph.shortest_path("hurricane", "joey")))
    if set1_graph.has_path("hurricane", "isis"):
        print "Relatedness between hurricane and isis = " +  str(set1_graph.getRelatedness(set1_graph.shortest_path("hurricane", "isis")))
    if set1_graph.has_path("hurricane", "sister"):
        print "Relatedness between hurricane and sister = " +  str(set1_graph.getRelatedness(set1_graph.shortest_path("hurricane", "sister")))
    if set1_graph.has_path("joey", "shelter"):
        print "Relatedness between joey and shelter = " +  str(set1_graph.getRelatedness(set1_graph.shortest_path("joey", "shelter")))
    
    print "-" * 50
    print "New document set"
    print "-" * 50
    set2_graph = TermGraph()
    set2 = getDocumentSet("set2")
    for document in set2:
        addDocumentToGraph(set2_graph, document)
    if set2_graph.has_path("hurricane", "isis"):
        print "Relatedness between hurricane and isis = " + str(set2_graph.getRelatedness(set2_graph.shortest_path("hurricane", "isis")))
    if set1_graph.has_path("hurricane", "mozambique"):
        print "Relatedness between hurricane and mozambique = " + str(set2_graph.getRelatedness(set2_graph.shortest_path("hurricane", "mozambique")))
    if set1_graph.has_path("hurricane", "romance"):
        print "Relatedness between hurricane and romance = " + str(set2_graph.getRelatedness(set2_graph.shortest_path("hurricane", "romance")))
    
    print "-" * 50
    print "New document set"
    print "-" * 50
    
    set3_graph = TermGraph()    
    set3 = getDocumentSet("set3")
    for document in set3:
        addDocumentToGraph(set3_graph, document)
    if set3_graph.has_path("forever", "young"):
        print "Relatedness between forever and young = " + str(set3_graph.getRelatedness(set3_graph.shortest_path("forever", "young")))
    if set3_graph.has_path("forever", "guards"):
        print "Relatedness between forever and guards = " + str(set3_graph.getRelatedness(set3_graph.shortest_path("forever", "guards")))
    if set3_graph.has_path("forever", "power"):
        print "Relatedness between forever and power = " + str(set3_graph.getRelatedness(set3_graph.shortest_path("forever", "power")))
    if set3_graph.has_path("tales", "there"):
        print "Relatedness between tales and there = " + str(set3_graph.getRelatedness(set3_graph.shortest_path("forever", "there")))
    if set3_graph.has_path("tales", "young"):
        print "Relatedness between tales and young = " + str(set3_graph.getRelatedness(set3_graph.shortest_path("tales", "young")))
    
    

if __name__ == '__main__':
    main()