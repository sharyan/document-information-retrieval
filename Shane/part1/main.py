'''
Created on 1 Oct 2013

@author: Shane
'''
from __future__ import division

import nltk.stem
from nltk.corpus import stopwords
from nltk.stem.lancaster import LancasterStemmer
import os
import re
import string
from nltk.stem.snowball import SnowballStemmer, PorterStemmer

path = "text/"

########  ######## ##     ##  #######  ##     ## ########
##     ## ##       ###   ### ##     ## ##     ## ##
##     ## ##       #### #### ##     ## ##     ## ##
########  ######   ## ### ## ##     ## ##     ## ######
##   ##   ##       ##     ## ##     ##  ##   ##  ##
##    ##  ##       ##     ## ##     ##   ## ##   ##
##     ## ######## ##     ##  #######     ###    ########
######## ##     ## ##       ##        ######  ########  #######  ########
##       ##     ## ##       ##       ##    ##    ##    ##     ## ##     ##
##       ##     ## ##       ##       ##          ##    ##     ## ##     ##
######   ##     ## ##       ##        ######     ##    ##     ## ########
##       ##     ## ##       ##             ##    ##    ##     ## ##
##       ##     ## ##       ##       ##    ##    ##    ##     ## ##
##        #######  ######## ########  ######     ##     #######  ##
def remove_fullstops(text):
    rx = re.compile('[%s]' % re.escape(string.punctuation))
    new_text = rx.sub(" ", text)
    return new_text

##     ##    ###    #### ##    ##
###   ###   ## ##    ##  ###   ##
#### ####  ##   ##   ##  ####  ##
## ### ## ##     ##  ##  ## ## ##
##     ## #########  ##  ##  ####
##     ## ##     ##  ##  ##   ###
##     ## ##     ## #### ##    ##
def main():
    for inputFile in os.listdir(path):
        if os.path.isfile(path + '/' + inputFile):
            print "-" * 50
            print "Working on stop word removal for " + inputFile
            f = open(path + '/' + inputFile, "r")
            text = ' '.join(f.readlines()).lower()
            f.close()
            # Now, process text
            # Already lower case, need to stem and remove stopwords
            stpwords = stopwords.words('english')
            clean_text = nltk.clean_html(text)

            tokens = nltk.word_tokenize(clean_text)

            doc = [word for word in tokens if word not in stpwords and len(word) > 3]

            sentence_before = ' '.join(map(str, doc))
            len_before = len(sentence_before)
            print "Length before stemming " + str(len_before)

            stemmer = PorterStemmer() # Choose a language
            doc = [stemmer.stem(word) for word in doc]
            text = nltk.Text(doc)
            sentence_after = ' '.join(map(str,text.tokens))
            len_after = len(sentence_after)
            print "Length after stemming " + str(len_after)

            print "Stemming compression ratio = " + str(len_before/len_after)


######## ########
##       ##     ##
##       ##     ##
######   ########
##       ##
##       ##
######## ##
if __name__ == "__main__":
    main()
