'''
Created on 1 Oct 2013

@author: Shane
'''
from __future__ import division

import nltk
from nltk.corpus import stopwords
from nltk.stem.lancaster import LancasterStemmer
import os
import re
import string

path = "text/"


def remove_fullstops(text):
    rx = re.compile('[%s]' % re.escape(string.punctuation))
    new_text = rx.sub(" ", text)
    return new_text


def main():
    for inputFile in os.listdir(path):
        if os.path.isfile(path + '/' + inputFile):
            print "-" * 50
            print "Working on stop word removal for " + inputFile
            f = open(path + '/' + inputFile, "r")
            text = ' '.join(f.readlines()).lower()
            f.close()
            # Now, process text
            # Already lower case, need to stem and remove stopwords
            stpwords = stopwords.words('english')
            clean_text = nltk.clean_html(text)

            without_fullstops = remove_fullstops(clean_text)

            len_before = len(without_fullstops)

            print "Size of document without fullspaces before stopword removal " + str(len_before)

            tokens = nltk.word_tokenize(without_fullstops)

            doc = [word for word in tokens if word not in stpwords and len(word) > 3]

            len_after = len(doc)
            print "Size of document without fullspaces after stopword removal " + str(len_after)

            ratio = len_before / len_after
            print "Compression ratio for stop word removal " + str(ratio)

            sentence_before = ' '.join(map(str, doc))
            print sentence_before
            len_before = len(sentence_before)
            print "Length before stemming " + str(len_before)

            stemmer = LancasterStemmer()
            doc = [stemmer.stem(word) for word in doc]
            text = nltk.Text(doc)
            sentence_after = ' '.join(map(str, text.tokens))
            len_after = len(sentence_after)
            print "Length after stemming " + str(len_after)

            print sentence_after


if __name__ == "__main__":
    main()
